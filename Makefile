SHELL := /bin/bash
.SHELLFLAGS = -e -c
.ONESHELL:

.PHONY: clean freeze

PRJ_NAME = staticgen.py

.venv: .venv/bin/activate

.venv/bin/activate: requirements.txt
	test -d .venv || python3 -m venv .venv/ --prompt $(PRJ_NAME)
	source .venv/bin/activate
	pip install --upgrade pip setuptools
	pip install wheel
	test -f requirements.txt && pip install -r requirements.txt
	deactivate

freeze: .venv
	source .venv/bin/activate
	pip freeze | grep -v "pkg-resources" > requirements.txt
	deactivate

build: .venv
	source .venv/bin/activate
	python setup.py sdist bdist_wheel
	deactivate

upload_test_package: build
	source .venv/bin/activate
	pip install --upgrade twine
	twine upload --repository testpypi dist/*
	deactivate

clean:
	find . -name "*.pyc" -type f -delete
	find . -name __pycache__ -type d -exec rm -rf {} 2> /dev/null +
	find . -name "*.egg-info" -type d -exec rm -rf {} 2> /dev/null +
	find . -type d -empty -delete
	test ! -d .venv || rm -r .venv
	test ! -d dist || rm -r dist
