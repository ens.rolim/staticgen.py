# staticgen.py

Simple and light static website / blog generator, written for Python programmers, using multiprocessing to better employ the computational resources of the most modern machines.


Icon made by [Freepik](https://www.freepik.com) from [Flaticon](https://www.flaticon.com) is licensed by [Creative Commons BY 3.0](http://creativecommons.org/licenses/by/3.0/).
