#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at https://mozilla.org/MPL/2.0/.


class SourceFile(object):
    def __init__(self, filename, encoding='utf-8'):
        self.encoding = encoding
        self.filename = filename
        self.mtime = os.path.getmtime(filename)

class Page(object):
    pass
