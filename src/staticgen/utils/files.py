#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at https://mozilla.org/MPL/2.0/.


import os, fnmatch, collections


def match_filter(file, filters=[]):
    for pattern in filters:
        if fnmatch.fnmatch(file, pattern):
            return True
    return False


class FileList(object):
    def __init__(self, folder, filters=['*']):
        self.folder = folder
        self.filters = filters
    
    def __len__(self):
        d = collections.deque(enumerate(self.gen_filelist(), 1), maxlen=1)
        return d[0][0] if d else 0
    
    def __iter__(self):
        return self.gen_filelist()
        
    def gen_filelist(self):
        if isinstance(self.folder, FileList):
            gen = self.folder.gen_filelist()
            yield from (f for f in gen if match_filter(f, self.filters))
        else:
            for r, d, f in os.walk(self.folder):
                for file in f:
                    filename = os.path.join(r, file)
                    if match_filter(filename, self.filters):
                        yield filename
