#!/usr/bin/env python3
# -*- coding: utf-8 -*-

## This Source Code Form is subject to the terms of the Mozilla Public
## License, v. 2.0. If a copy of the MPL was not distributed with this
## file, You can obtain one at https://mozilla.org/MPL/2.0/.


"""
# staticgen.py

Simple and light static website / blog generator, written for Python
programmers, using multiprocessing to better employ the computational
resources of the most modern machines

:copyright: (c) 2019 by Eduardo Rolim.
:license: MPL 2.0, see LICENSE for more details.
"""

from .generators import StaticGenerator

__all__ = ['StaticGenerator']

__author__ = 'Eduardo Rolim'
__license__ = 'Mozilla Public License v2.0'
__version__ = '0.0.1'
